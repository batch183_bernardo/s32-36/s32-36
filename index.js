// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

// Create server
const app = express();
const port = 4000;

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@course-booking.ubhpmvq.mongodb.net/course-booking-app?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set notification for connection success or failure
let db = mongoose.connection;

// Notify on Error
db.on("error", console.error.bind(console, "connection error"));
// Notify on Successfully Connected
db.once("open", () => console.log("We're connected to the cloud database."));

//Routes for our API
//localhost:4000/users
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// Listening to port
// This syntax will allow flexibility when using the application locally or as a hosted application.
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`)
})